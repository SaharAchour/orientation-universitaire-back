import { IsString, Matches, MaxLength, MinLength } from 'class-validator';

export class AuthCredentialsDto {
  name: string;

  firstname: string;

  role: string;

  status: string;

  sexe: string;

  adress: string;

  @MinLength(8)
  password: string;

  salt: String;
}
