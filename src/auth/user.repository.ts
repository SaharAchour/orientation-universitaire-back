import { EntityRepository, Repository } from 'typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from './user.entity';
import * as bcrypt from 'bcrypt';
import { Script } from 'node:vm';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async signUp(authCredentialsDto: AuthCredentialsDto) {
    const {
      name,
      firstname,
      role,
      status,
      sexe,
      adress,
      password,
    } = authCredentialsDto;

    const salt = await bcrypt.genSalt();

    const user = new User();
    user.name = name;
    user.firstname = firstname;
    user.password = await this.hashPassword(password, salt);
    user.role = role;
    user.status = status;
    user.sexe = sexe;
    user.adress = adress;
    await user.save();
  }

  private async hashPassword(password: string, salt: string): Promise<String> {
    return bcrypt.hash(password, salt);
  }

  async validateUserPassword(
    authCredentialsDto: AuthCredentialsDto,
  ): Promise<String> {
    const {
      name,
      firstname,
      role,
      status,
      sexe,
      adress,
      password,
      salt,
    } = authCredentialsDto;
    const user = await this.findOne({ adress });

    if (user && (await user.validatePassword(password))) {
      return user.adress;
    } else {
      return null;
    }
  }
}
