import { TypeOrmModule } from '@nestjs/typeorm';

export const typeOrmConfig: TypeOrmModule = {
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: '123456',
  database: 'orientationuniversitaire',
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
};
